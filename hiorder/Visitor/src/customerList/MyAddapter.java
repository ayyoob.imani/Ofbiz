package customerList;

import java.util.ArrayList;

import com.visitor.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MyAddapter extends ArrayAdapter<ItemCustomer> 
{
	private final Context context;
	private final ArrayList<ItemCustomer> itemsArraylist;
	public MyAddapter(Context context , ArrayList<ItemCustomer> itemsArraylist)
	{
		super(context, R.layout.customer_list_row, itemsArraylist);
		this.context = context;
		this.itemsArraylist = itemsArraylist;
		
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		Log.d("getView", "1");
		// 1. Create inflater 
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		// 2. Get rowView from inflater
		View rowView = inflater.inflate(R.layout.customer_list_row, parent, false);
		
		 // 3. Get the two text view from the rowView
		TextView NameView = (TextView) rowView.findViewById(R.id.contact_name_info);
		TextView phoneView = (TextView) rowView.findViewById(R.id.contact_phone_info);ImageView pictureView = (ImageView) rowView.findViewById(R.id.contact_image);
		Log.d("getView", "2");
		// 4. Set the text for textView 
		NameView.setText(itemsArraylist.get(position).getName());
		phoneView.setText(itemsArraylist.get(position).getPhoneNumber());
		Log.d("getView", "3");
		ImageView temp = itemsArraylist.get(position).getImage();
		Bitmap viewBitmap = Bitmap.createBitmap(temp.getWidth(),temp.getHeight(),Bitmap.Config.ARGB_8888);//i is imageview whch u want to convert in bitmap
		Log.d("getView", "4");
		Canvas canvas = new Canvas(viewBitmap);
		Log.d("getView", "5");
		temp.draw(canvas);
		
		Log.d("getView", "6");
		pictureView.setImageBitmap(viewBitmap);
		Log.d("getView", "7");
		

		//5. retrn rowView
		return rowView;
		
	}

}
