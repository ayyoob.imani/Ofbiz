package customerList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.visitor.R;
import com.visitor.R.id;
import com.visitor.R.layout;

import android.app.ListActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View.OnClickListener;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SimpleAdapter;

public class CustomerListActivity extends ListActivity
{
	private final String TEXT_KEY_1 = "text1";
	private final String TEXT_KEY_2 = "text2";
	private final String IMG_KEY = "img";
	
	ImageButton refresh;
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.customer_list);
		findViewById(R.id.refresh_button).setVisibility(View.VISIBLE);
		findViewById(R.id.progress).setVisibility(View.GONE);
		
		refresh = (ImageButton)findViewById(R.id.refresh_button);
		
		List<Map<String, Object>> resourceNames = new ArrayList<Map<String, Object>>();

		generateData(resourceNames);

		// adapter that will build the list items
		SimpleAdapter adapter = new SimpleAdapter(this, resourceNames,
				R.layout.customer_list_row, new String[] { TEXT_KEY_1, IMG_KEY,
						TEXT_KEY_2 }, new int[] { R.id.contact_name_info, R.id.contact_image,
						R.id.contact_phone_info });

		setListAdapter(adapter);

		
		//MyAddapter adapter = new MyAddapter(this, generateData());
		//setListAdapter(adapter);
		addOnclickLister();
	}
	private void generateData(List<Map<String, Object>> resourceNames) {
		// number of list items
		int NUM_ITEMS = 4;
		Map<String, Object> data;		
		String[] names ={"Mojtaba Eslami","Ayoob Imani","sina Ghazizade","Mohamad Jalali"};
		String[] phones ={"09126956072","09331348341","09352204367","09133879440"};
		Integer[] imgIds = { R.drawable.person_icon, R.drawable.person_icon,
				R.drawable.person_icon, R.drawable.person_icon};
		for (int i = 0; i < NUM_ITEMS; i++) {
			data = new HashMap<String, Object>();
			data.put(TEXT_KEY_1, names[i]);
			data.put(IMG_KEY, imgIds[i]);
			data.put(TEXT_KEY_2, phones[i]);
			resourceNames.add(data);
		}
	}
	
	protected void addOnclickLister()
	{
		refresh.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				findViewById(R.id.refresh_button).setVisibility(View.INVISIBLE);
				findViewById(R.id.progress).setVisibility(View.VISIBLE);
				 
			}
		});
		
	}
	

}
