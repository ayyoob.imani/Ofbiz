<#--
  Licensed to the Apache Software Foundation (ASF) under one
  or more contributor license agreements.  See the NOTICE file
  distributed with this work for additional information
  regarding copyright ownership.  The ASF licenses this file
  to you under the Apache License, Version 2.0 (the
  "License"); you may not use this file except in compliance
  with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an
  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  KIND, either express or implied.  See the License for the
  specific language governing permissions and limitations
  under the License.
  -->
  
<#--
<script language="javascript" type="text/javascript" src="<@ofbizContentUrl>/images/jquery/plugins/jsTree/jquery.jstree.js</@ofbizContentUrl>"></script>
<script type="text/javascript" src="<@ofbizContentUrl>/images/jquery/ui/development-bundle/external/jquery.cookie.js</@ofbizContentUrl>"></script>
-->

<script language="javascript" type="text/javascript" src="<@ofbizContentUrl>/hadid/js/javascripts/demo.js</@ofbizContentUrl>"></script>
<script language="javascript" type="text/javascript" src="<@ofbizContentUrl>/hadid/js/javascripts/theme.js</@ofbizContentUrl>"></script>
<script language="javascript" type="text/javascript" src="<@ofbizContentUrl>/hadid/js/javascripts/jquery/jquery2.min.js</@ofbizContentUrl>"></script>
<link rel="stylesheet" href="<@ofbizContentUrl>/hadid/light-theme.css</@ofbizContentUrl>" type="text/css">

<script type="text/javascript">

<#-------------------------------------------------------------------------------------callDocument function-->
    function callDocument(id, parentCategoryStr) {
        var checkUrl = '<@ofbizUrl>productCategoryList</@ofbizUrl>';
        if(checkUrl.search("http"))
            var ajaxUrl = '<@ofbizUrl>productCategoryList</@ofbizUrl>';
        else
            var ajaxUrl = '<@ofbizUrl>productCategoryListSecure</@ofbizUrl>';

        //jQuerry Ajax Request
        jQuery.ajax({
            url: ajaxUrl,
            type: 'POST',
            data: {"category_id" : id, "parentCategoryStr" : parentCategoryStr},
            error: function(msg) {
                alert("An error occured loading content! : " + msg);
            },
            success: function(msg) {
                jQuery('#div3').html(msg);
            }
        });
     }


</script>




<form class="reg-page nopadding">
<#--
	<div class="reg-header">
	    <h3 class="pagination-centered">${uiLabelMap.ProductCategories}</h3>
	</div>
-->
	<nav id="main-nav">
	  <div class="navigation">
	
	<#assign level=0>	
	
	<#if (requestAttributes.topLevelList)?exists>
    <#assign topLevelList = requestAttributes.topLevelList>
  </#if>
  <#if (topLevelList?has_content)>
	  <ul class="nav nav-stacked">
	  
	    <@fillTree rootCat=completedTree treeLevel=level/>
	  </ul>
  </#if>
  
  
  
  <#macro fillTree rootCat treeLevel>
  <#if (rootCat?has_content)>
    <#list rootCat?sort_by("productCategoryId") as root>
    
         <li id="${root.productCategoryId}" class>
         	
         	<#if root.child?has_content>
         	  <a href="javascript: void(0);" onClick="callDocument('${root.productCategoryId}', '${root.parentCategoryId}')" class="dropdown-collapse">
         	    <#if treeLevel==0>
         	     <i class="icon-cogs"></i>
         	    <#else>
         	     <i class="icon-folder-open-alt"></i>
         	    </#if>
         	  	<span><#if root.categoryName?exists>${root.categoryName?js_string}<#elseif root.categoryDescription?exists>${root.categoryDescription?js_string}<#else>${root.productCategoryId?js_string}</#if></span>
              	<i class="icon-angle-down angle-down"></i>
              </a>
	          
	          <ul class="nav nav-stacked" style="display: none;">
                <@fillTree rootCat=root.child treeLevel=1/>
              </ul>
	        <#else>
	        
	         <a href="javascript: void(0);" onClick="callDocument('${root.productCategoryId}', '${root.parentCategoryId}')" class="dropdown-collapse">
	           <#if treeLevel!=0>
	           	<i class="icon-caret-left"></i>
	           <#else >
	           	<i class="icon-cogs"></i>
	           </#if>
	           <span> 
	           	 <#if root.categoryName?exists>${root.categoryName?js_string}<#elseif root.categoryDescription?exists>${root.categoryDescription?js_string}<#else>${root.productCategoryId?js_string}</#if>
	           </span>
	           
	         </a>
	        </#if>
            </li>
         
    </#list>
  </#if>
  </#macro>
    
      
    </div>
  </nav>   
	    
	    <#--<div  id="tree" >
	    </div>-->
    
</form>
