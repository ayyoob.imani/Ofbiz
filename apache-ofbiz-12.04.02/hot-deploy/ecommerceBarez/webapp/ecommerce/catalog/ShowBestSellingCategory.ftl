<#--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->

<#if productCategoryList?has_content>
    <h1>Popular Categories</h1>
    <div class="productsummary-container matrix">
	<div id="myCarousel" class="carousel slide">
            <div class="carousel-inner">
       
        

            <#list productCategoryList as childCategoryList>
                   
                   <#assign cateCount = 0/>
                   <#assign ActiveImgCount = 0/>
                   <#list childCategoryList as productCategory>
                       <#if (cateCount > 2)>
                            
                            <#assign cateCount = 0/>
                       </#if>
                       <#assign productCategoryId = productCategory.productCategoryId/>
                       <#assign categoryImageUrl = "/images/defaultImage.jpg">
                       <#assign productCategoryMembers = delegator.findByAnd("ProductCategoryAndMember", Static["org.ofbiz.base.util.UtilMisc"].toMap("productCategoryId", productCategoryId), Static["org.ofbiz.base.util.UtilMisc"].toList("-quantity"))>
                       <#if productCategory.categoryImageUrl?has_content>
                            <#assign categoryImageUrl = productCategory.categoryImageUrl/>
                       <#elseif productCategoryMembers?has_content>
                            <#assign productCategoryMember = Static["org.ofbiz.entity.util.EntityUtil"].getFirst(productCategoryMembers)/>
                            <#assign product = delegator.findByPrimaryKey("Product", Static["org.ofbiz.base.util.UtilMisc"].toMap("productId", productCategoryMember.productId))/>
                            <#if product.largImageUrl?has_content>
                                <#assign categoryImageUrl = product.largImageUrl/>
                                
                            </#if>
                       </#if>
					   
                        <div  class="item 
                        <#if ActiveImgCount == 0 >
                           active
                        </#if>
                        ">
						
						
						<img src="${categoryImageUrl}" alt="">
						<div class="carousel-caption">
						<h4>${productCategory.categoryName!productCategoryId}</h4>
						
						<div class="productinfo">
                                    <ul>
                                    <#if productCategoryMembers?exists>
                                        <#assign i = 0/>
                                        <#list productCategoryMembers as productCategoryMember>
                                            <#if (i > 2)>
                                                <#if productCategoryMembers[i]?has_content>
                                                    <li><a class="linktext" href="<@ofbizCatalogAltUrl productCategoryId=productCategoryId/>">
                                                        <span>More...</span>
                                                    </a></li>
                                                </#if>
                                                <#break>
                                            </#if>
                                            <#if productCategoryMember?has_content>
                                                <#assign product = delegator.findByPrimaryKey("Product", Static["org.ofbiz.base.util.UtilMisc"].toMap("productId", productCategoryMember.productId))>
                                                <li class="browsecategorytext">
                                                    <a class="linktext" href="<@ofbizCatalogAltUrl productCategoryId="PROMOTIONS" productId="${product.productId}"/>">
                                                        ${product.productName!product.productId}
                                                    </a>
                                                </li>
                                            </#if>
                                            <#assign i = i+1/>
                                        </#list>
                                    </#if>
                                    </ul>
                                </div>
						
						</div>
						</div>
                          
                        <#assign ActiveImgCount = ActiveImgCount +1 />
                        <#assign cateCount = cateCount + 1/>
						
                 </#list>
               
                 
               </#list>
            
        
      </div>
	  <a class="left carousel-control" href="#myCarousel" data-slide="prev">�</a>
	  <a class="right carousel-control" href="#myCarousel" data-slide="next">�</a>
    </div>
	</div>
</#if>
