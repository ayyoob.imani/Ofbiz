<#--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->
<div class="reg-page promote halfwidth margin-right-220">
    <div class="screenlet-title-bar">
        <h3>${uiLabelMap.CommonContactUs}</h3>
    </div>
    <script type="text/javascript" language="JavaScript">
    <!--
        function reloadCaptcha(fieldName) {
            var captchaUri = "<@ofbizUrl>captcha.jpg?captchaCodeId=" + fieldName + "&amp;unique=_PLACEHOLDER_</@ofbizUrl>";
            var unique = Date.now();
            captchaUri = captchaUri.replace("_PLACEHOLDER_", unique);
            document.getElementById(fieldName).src = captchaUri;
        }
    //-->
    </script>
    <div class="screenlet-body-contactus">
        <form id="contactForm" method="post" action="<@ofbizUrl>submitAnonContact</@ofbizUrl>">
            <input type="hidden" name="partyIdFrom" value="${(userLogin.partyId)?if_exists}" />
            <input type="hidden" name="partyIdTo" value="${productStore.payToPartyId?if_exists}"/>
            <input type="hidden" name="contactMechTypeId" value="WEB_ADDRESS" />
            <input type="hidden" name="communicationEventTypeId" value="WEB_SITE_COMMUNICATI" />
            <input type="hidden" name="productStoreId" value="${productStore.productStoreId}" />
            <input type="hidden" name="emailType" value="CONT_NOTI_EMAIL" />
            
                
                       <div>${uiLabelMap.EcommerceSubject}</div>
                       <input type="text" name="subject" id="subject" class="required" value="${requestParameters.subject?if_exists}"/><span>*</span>
                    
                   
                       <div>${uiLabelMap.CommonMessage}</div>
                       <textarea name="content" id="message" class="required" cols="50" rows="5">${requestParameters.content?if_exists}</textarea><span>*</span>
                    
                    
                       <div>${uiLabelMap.EcommerceFormFieldTitle_emailAddress}</div>
                       <input type="text" name="emailAddress" id="emailAddress" class="direction-ltr required" value="${requestParameters.emailAddress?if_exists}"/><span>*</span>
                    
                    
                       <div>${uiLabelMap.PartyFirstName}</div>
                       <input type="text" name="firstName" id="firstName" class="required" value="${requestParameters.firstName?if_exists}"/>
                    
                    
                       <div >${uiLabelMap.PartyLastName}</div>
                       <input type="text" name="lastName" id="lastName " class=" required" value="${requestParameters.lastName?if_exists}"/>
                    
                    
                       <div>${uiLabelMap.CommonCaptchaCode}</div>
                       <div><img id="captchaImage" src="<@ofbizUrl>captcha.jpg?captchaCodeId=captchaImage&amp;unique=${nowTimestamp.getTime()}</@ofbizUrl>" alt="" /></div><a href="javascript:reloadCaptcha('captchaImage');">${uiLabelMap.CommonReloadCaptchaCode}</a>
                    
                    
                       <div>${uiLabelMap.CommonVerifyCaptchaCode}</div>
                       <input type="text" autocomplete="off" maxlength="30" size="23" name="captcha"/><span>*</span>
                    
                    
                       <div class="clear margin-bottom-10"></div>
					   
                       <input type="submit" class="btn btn-primary" value="${uiLabelMap.CommonSubmit}" />
                   
                
            
        </form>
    </div>
</div>
