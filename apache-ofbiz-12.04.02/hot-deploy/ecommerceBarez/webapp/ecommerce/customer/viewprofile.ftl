<#--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->

<#if party?exists>
<div class="span12">
<#-- Main Heading -->
<div class="span12 solid-line">

      <h2 class="span9 ">${uiLabelMap.PartyTheProfileOf}
        <#if person?exists>
          ${person.personalTitle?if_exists}
          ${person.firstName?if_exists}
          ${person.middleName?if_exists}
          ${person.lastName?if_exists}
          ${person.suffix?if_exists}
        <#else>
          "${uiLabelMap.PartyNewUser}"
        </#if>
      </h2>

<div class=" pull-left">
      <#if showOld>
        <a href="<@ofbizUrl>viewprofile</@ofbizUrl>" class="btn btn-primary">${uiLabelMap.PartyHideOld}</a>
    <#--  <#else>
        <a href="<@ofbizUrl>viewprofile?SHOW_OLD=true</@ofbizUrl>" class="btn btn-primary">${uiLabelMap.PartyShowOld}</a> -->
      </#if>
      <#if (productStore.enableDigProdUpload)?if_exists == "Y">
        <a href="<@ofbizUrl>digitalproductlist</@ofbizUrl>" class="btn btn-primary">${uiLabelMap.EcommerceDigitalProductUpload}</a>
      </#if>
</div>
</div>
</div>
<br><br>


<div class="span12">

<form class="reg-page promote">
    <div class="reg-header">
		<h3>${uiLabelMap.PartyPersonalInformation}</h3>
	</div>	
		 
  <div class="screenlet-body">
    <#if person?exists>
    <div>
      <table width="100%"  class="table ">
        <tr>
	         <td align="right">
	         	${uiLabelMap.PartyName}
	         </td>
	          
	         <td>
	              ${person.personalTitle?if_exists}
	              ${person.firstName?if_exists}
	              ${person.middleName?if_exists}
	              ${person.lastName?if_exists}
	              ${person.suffix?if_exists}
	         </td>
	         
	         <td class="">
	          	<a href="<@ofbizUrl>editperson</@ofbizUrl>" class="btn btn-primary">
			    	<#if person?exists>${uiLabelMap.CommonUpdate}<#else>${uiLabelMap.CommonCreate}</#if></a>
			 </td>
        </tr>
        
        <tr>
	        <td align="right" valign="top">${uiLabelMap.EcommercePhoneNumber}</td>
	        
	        <td valign="top">${userLogin.userLoginId}</td>
	        
	        <td class="">
	        	<a href="<@ofbizUrl>changepassword</@ofbizUrl>" class="btn btn-primary">${uiLabelMap.PartyChangePassword}</a>
	        </td>
        </tr>
        <#--
      <#if person.nickname?has_content><tr><td align="right">${uiLabelMap.PartyNickName}</td><td>${person.nickname}</td></tr></#if>
      <#if person.gender?has_content><tr><td align="right">${uiLabelMap.PartyGender}</td><td>${person.gender}</td></tr></#if>
      <#if person.birthDate?exists><tr><td align="right">${uiLabelMap.PartyBirthDate}</td><td>${person.birthDate.toString()}</td></tr></#if>
      <#if person.height?exists><tr><td align="right">${uiLabelMap.PartyHeight}</td><td>${person.height}</td></tr></#if>
      <#if person.weight?exists><tr><td align="right">${uiLabelMap.PartyWeight}</td><td>${person.weight}</td></tr></#if>
      <#if person.mothersMaidenName?has_content><tr><td align="right">${uiLabelMap.PartyMaidenName}</td><td>${person.mothersMaidenName}</td></tr></#if>
      <#if person.maritalStatus?has_content><tr><td align="right">${uiLabelMap.PartyMaritalStatus}</td><td>${person.maritalStatus}</td></tr></#if>
      <#if person.socialSecurityNumber?has_content><tr><td align="right">${uiLabelMap.PartySocialSecurityNumber}</td><td>${person.socialSecurityNumber}</td></tr></#if>
      <#if person.passportNumber?has_content><tr><td align="right">${uiLabelMap.PartyPassportNumber}</td><td>${person.passportNumber}</td></tr></#if>
      <#if person.passportExpireDate?exists><tr><td align="right">${uiLabelMap.PartyPassportExpireDate}</td><td>${person.passportExpireDate.toString()}</td></tr></#if>
      <#if person.totalYearsWorkExperience?exists><tr><td align="right">${uiLabelMap.PartyYearsWork}</td><td>${person.totalYearsWorkExperience}</td></tr></#if>
      <#if person.comments?has_content><tr><td align="right">${uiLabelMap.CommonComments}</td><td>${person.comments}</td></tr></#if>
      -->
      </table>
    </div>
    <#else>
      <label>${uiLabelMap.PartyPersonalInformationNotFound}</label>
    </#if>
  </div>
</form>

</div>


<#-- ============================================================= -->
<#-->
<div class="span5 pull-left">
<form class="reg-page">
  <h3 class="span3">${uiLabelMap.CommonUsername} &amp; ${uiLabelMap.CommonPassword}</h3>
  <div class="pull-left">
    <a href="<@ofbizUrl>changepassword</@ofbizUrl>" class="btn btn-primary">${uiLabelMap.PartyChangePassword}</a>
  </div>
  
  <div class="screenlet-body">
    <table width="100%" border="0" cellpadding="1" class="table table-bordered">
      <tr>
        <td align="right" valign="top">${uiLabelMap.CommonUsername}</td>
        <td>&nbsp;</td>
        <td valign="top">${userLogin.userLoginId}</td>
      </tr>
    </table>
  </div>
</form>
</div>

-->

<#-- ============================================================= -->
<div class="span12">
<form class="reg-page promote">
	  <div class="reg-header">
		<h3 >${uiLabelMap.PartyContactInformation}</h3>
	  <div class="stick-to-top">
	    <a href="<@ofbizUrl>editcontactmech</@ofbizUrl>" class="btn btn-primary">${uiLabelMap.CommonCreateNew}</a>
	  </div>
	</div>
	 
  <div class="screenlet-body">
  <#if partyContactMechValueMaps?has_content>
    <table  class="table table-bordered">
      <tr valign="bottom">
        <th>${uiLabelMap.PartyContactType}</th>
        <th></th>
        <th>${uiLabelMap.CommonInformation}</th>
        <#-- <th colspan="2">${uiLabelMap.PartySolicitingOk}?</th> -->
        <th></th>
        <th></th>
      </tr>
      <#list partyContactMechValueMaps as partyContactMechValueMap>
        <#assign contactMech = partyContactMechValueMap.contactMech?if_exists />
        <#assign contactMechType = partyContactMechValueMap.contactMechType?if_exists />
        <#assign partyContactMech = partyContactMechValueMap.partyContactMech?if_exists />
          <tr><td colspan="6"></td></tr>
          <tr>
            <td align="right" valign="top">
              ${contactMechType.get("description",locale)}
            </td>
            <td>&nbsp;</td>
            <td valign="top">
              <#list partyContactMechValueMap.partyContactMechPurposes?if_exists as partyContactMechPurpose>
                <#assign contactMechPurposeType = partyContactMechPurpose.getRelatedOneCache("ContactMechPurposeType") />
                <div class="tabletext">
                  <#if contactMechPurposeType?exists>
                    ${contactMechPurposeType.get("description",locale)}
                    <#if contactMechPurposeType.contactMechPurposeTypeId == "SHIPPING_LOCATION" && (profiledefs.defaultShipAddr)?default("") == contactMech.contactMechId>
                      <span class="buttontextdisabled">${uiLabelMap.EcommerceIsDefault}</span>
                    <#elseif contactMechPurposeType.contactMechPurposeTypeId == "SHIPPING_LOCATION">
                      <form name="defaultShippingAddressForm" method="post" action="<@ofbizUrl>setprofiledefault/viewprofile</@ofbizUrl>">
                        <input type="hidden" name="productStoreId" value="${productStoreId}" />
                        <input type="hidden" name="defaultShipAddr" value="${contactMech.contactMechId}" />
                        <input type="hidden" name="partyId" value="${party.partyId}" />
                        <input type="submit" value="${uiLabelMap.EcommerceSetDefault}" class="button" />
                      </form>
                    </#if>
                  <#else>
                    ${uiLabelMap.PartyPurposeTypeNotFound}: "${partyContactMechPurpose.contactMechPurposeTypeId}"
                  </#if>
                  <#if partyContactMechPurpose.thruDate?exists>(${uiLabelMap.CommonExpire}:${partyContactMechPurpose.thruDate.toString()})</#if>
                </div>
              </#list>
              <#if contactMech.contactMechTypeId?if_exists = "POSTAL_ADDRESS">
                <#assign postalAddress = partyContactMechValueMap.postalAddress?if_exists />
                <div class="tabletext">
                  <#if postalAddress?exists>
                    <#if postalAddress.toName?has_content>${uiLabelMap.CommonTo}: ${postalAddress.toName}<br /></#if>
                    <#if postalAddress.attnName?has_content>${uiLabelMap.PartyAddrAttnName}: ${postalAddress.attnName}<br /></#if>
                    ${postalAddress.address1}<br />
                    <#if postalAddress.address2?has_content>${postalAddress.address2}<br /></#if>
                    ${postalAddress.city}<#if postalAddress.stateProvinceGeoId?has_content>,&nbsp;${postalAddress.stateProvinceGeoId}</#if>&nbsp;${postalAddress.postalCode?if_exists}
                    <#if postalAddress.countryGeoId?has_content><br />${postalAddress.countryGeoId}</#if>
                    <#if (!postalAddress.countryGeoId?has_content || postalAddress.countryGeoId?if_exists = "USA")>
                      <#assign addr1 = postalAddress.address1?if_exists />
                      <#if (addr1.indexOf(" ") > 0)>
                        <#assign addressNum = addr1.substring(0, addr1.indexOf(" ")) />
                        <#assign addressOther = addr1.substring(addr1.indexOf(" ")+1) />
                        <a target="_blank" href="${uiLabelMap.CommonLookupWhitepagesAddressLink}" class="linktext">(${uiLabelMap.CommonLookupWhitepages})</a>
                      </#if>
                    </#if>
                  <#else>
                    ${uiLabelMap.PartyPostalInformationNotFound}.
                  </#if>
                  </div>
              <#elseif contactMech.contactMechTypeId?if_exists = "TELECOM_NUMBER">
                <#assign telecomNumber = partyContactMechValueMap.telecomNumber?if_exists>
                <div class="tabletext">
                <#if telecomNumber?exists>
                  ${telecomNumber.countryCode?if_exists}
                  <#if telecomNumber.areaCode?has_content>${telecomNumber.areaCode}-</#if>${telecomNumber.contactNumber?if_exists}
                  <#if partyContactMech.extension?has_content>ext&nbsp;${partyContactMech.extension}</#if>
                  <#if (!telecomNumber.countryCode?has_content || telecomNumber.countryCode = "011")>
                    <a target="_blank" href="${uiLabelMap.CommonLookupAnywhoLink}" class="linktext">${uiLabelMap.CommonLookupAnywho}</a>
                    <a target="_blank" href="${uiLabelMap.CommonLookupWhitepagesTelNumberLink}" class="linktext">${uiLabelMap.CommonLookupWhitepages}</a>
                  </#if>
                <#else>
                  ${uiLabelMap.PartyPhoneNumberInfoNotFound}.
                </#if>
                </div>
              <#elseif contactMech.contactMechTypeId?if_exists = "EMAIL_ADDRESS">
                  ${contactMech.infoString}
                  <a href="mailto:${contactMech.infoString}" class="linktext">(${uiLabelMap.PartySendEmail})</a>
              <#elseif contactMech.contactMechTypeId?if_exists = "WEB_ADDRESS">
                <div class="tabletext">
                  ${contactMech.infoString}
                  <#assign openAddress = contactMech.infoString?if_exists />
                  <#if !openAddress.startsWith("http") && !openAddress.startsWith("HTTP")><#assign openAddress = "http://" + openAddress /></#if>
                  <a target="_blank" href="${openAddress}" class="linktext">(${uiLabelMap.CommonOpenNewWindow})</a>
                </div>
              <#else>
                ${contactMech.infoString?if_exists}
              </#if>
              <div class="tabletext">(${uiLabelMap.CommonUpdated}:&nbsp;${partyContactMech.fromDate.toString()})</div>
              <#if partyContactMech.thruDate?exists><div class="tabletext">${uiLabelMap.CommonDelete}:&nbsp;${partyContactMech.thruDate.toString()}</div></#if>
            </td>
            <#--
            <td align="center" valign="top"><div class="tabletext">(${partyContactMech.allowSolicitation?if_exists})</div></td>
            <td>&nbsp;</td>-->
            <td align="right" valign="top">
              <a href="<@ofbizUrl>editcontactmech?contactMechId=${contactMech.contactMechId}</@ofbizUrl>" class="button">${uiLabelMap.CommonUpdate}</a>
            </td>
            <td align="right" valign="top">
              <form name= "deleteContactMech_${contactMech.contactMechId}" method= "post" action= "<@ofbizUrl>deleteContactMech</@ofbizUrl>">
                <div>
                <input type= "hidden" name= "contactMechId" value= "${contactMech.contactMechId}"/>
                <a href='javascript:document.deleteContactMech_${contactMech.contactMechId}.submit()' class='button'>${uiLabelMap.CommonExpire}</a>
              </div>
              </form>
            </td>
          </tr>
      </#list>
    </table>
  <#else>
    <label>${uiLabelMap.PartyNoContactInformation}.</label><br />
  </#if>
  </div>
</form>
</div>


<#-- ============================================================= -->
<#-- only 5 messages will show; edit the ViewProfile.groovy to change this number -->
<#--${screens.render("component://ecommerce/widget/CustomerScreens.xml#messagelist-include")} -->
<#--
${screens.render("component://ecommerceBarez/widget/CustomerScreens.xml#FinAccountList-include")}
-->
<#-- Serialized Inventory Summary -->
<#-->
<div class="span12">
<form class="reg-page">
${screens.render('component://ecommerceBarez/widget/CustomerScreens.xml#SerializedInventorySummary')}
</form>
</div>
-->
<#-- Subscription Summary -->
<#--
${screens.render('component://ecommerceBarez/widget/CustomerScreens.xml#SubscriptionSummary')}
-->
<#-- Reviews -->
<#--
${screens.render('component://ecommerce/widget/CustomerScreens.xml#showProductReviews')}
-->
<#else>
    <h3>${uiLabelMap.PartyNoPartyForCurrentUserName}: ${userLogin.userLoginId}</h3>
</#if>

