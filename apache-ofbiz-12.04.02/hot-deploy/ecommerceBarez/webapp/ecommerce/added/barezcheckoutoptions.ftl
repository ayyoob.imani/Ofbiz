<#--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->

<script type="text/javascript">
//<![CDATA[
           $().ready( submitForm(document.checkoutInfoForm, 'SA', null) );
function submitForm(form, mode, value) {
	
    if (mode == "DN") {
        // done action; checkout
        form.action="<@ofbizUrl>checkout</@ofbizUrl>";
        form.submit();
    } else if (mode == "CS") {
        // continue shopping
        form.action="<@ofbizUrl>updateCheckoutOptions/showcart</@ofbizUrl>";
        form.submit();
    } else if (mode == "NA") {
        // new address
        form.action="<@ofbizUrl>updateCheckoutOptions/editcontactmech?DONE_PAGE=quickcheckout&partyId=${shoppingCart.getPartyId()}&preContactMechTypeId=POSTAL_ADDRESS&contactMechPurposeTypeId=SHIPPING_LOCATION</@ofbizUrl>";
        form.submit();
    } else if (mode == "EA") {
        // edit address
        form.action="<@ofbizUrl>updateCheckoutOptions/editcontactmech?DONE_PAGE=quickcheckout&partyId=${shoppingCart.getPartyId()}&contactMechId="+value+"</@ofbizUrl>";
        form.submit();
    } else if (mode == "NC") {
        // new credit card
        form.action="<@ofbizUrl>updateCheckoutOptions/editcreditcard?DONE_PAGE=quickcheckout&partyId=${shoppingCart.getPartyId()}</@ofbizUrl>";
        form.submit();
    } else if (mode == "EC") {
        // edit credit card
        form.action="<@ofbizUrl>updateCheckoutOptions/editcreditcard?DONE_PAGE=quickcheckout&partyId=${shoppingCart.getPartyId()}&paymentMethodId="+value+"</@ofbizUrl>";
        form.submit();
    } else if (mode == "GC") {
        // edit gift card
        form.action="<@ofbizUrl>updateCheckoutOptions/editgiftcard?DONE_PAGE=quickcheckout&partyId=${shoppingCart.getPartyId()}&paymentMethodId="+value+"</@ofbizUrl>";
        form.submit();
    } else if (mode == "NE") {
        // new eft account
        form.action="<@ofbizUrl>updateCheckoutOptions/editeftaccount?DONE_PAGE=quickcheckout&partyId=${shoppingCart.getPartyId()}</@ofbizUrl>";
        form.submit();
    } else if (mode == "EE") {
        // edit eft account
        form.action="<@ofbizUrl>updateCheckoutOptions/editeftaccount?DONE_PAGE=quickcheckout&partyId=${shoppingCart.getPartyId()}&paymentMethodId="+value+"</@ofbizUrl>";
        form.submit();
    } else if (mode == "SP") {
        // split payment
        form.action="<@ofbizUrl>updateCheckoutOptions/checkoutpayment?partyId=${shoppingCart.getPartyId()}</@ofbizUrl>";
        form.submit();
    } else if (mode == "SA") {
        // selected shipping address
        form.action="<@ofbizUrl>updateCheckoutOptions/quickcheckout</@ofbizUrl>";
        form.submit();
    } else if (mode == "SC") {
        // selected ship to party
        form.action="<@ofbizUrl>cartUpdateShipToCustomerParty</@ofbizUrl>";
        form.submit();
    }
}
//]]>
</script>

<#assign shipping = !shoppingCart.containAllWorkEffortCartItems()> <#-- contains items which need shipping? -->
<div class="span12">

<form method="post" name="checkoutInfoForm" style="margin:0;">
<div class="row">
<div class="span35 reg-page">
  <div class="reg-header"><h3>${uiLabelMap.OrderHowShallYouPay}</h3></div>
  <input type="hidden" name="checkoutpage" value="quick"/>
  <input type="hidden" name="BACK_PAGE" value="quickcheckout"/>
  
  <input type="hidden" name="may_split" value="false"/>
  <input type="hidden" name="is_gift" value="false"/>
  
    
  <#if productStorePaymentMethodTypeIdMap.EXT_OFFLINE?exists>
  
  	 <div class="row">
  		<div class="span1">
  			<img src="<@ofbizContentUrl>/images/barez/cache_on_delivery.jpg</@ofbizContentUrl>">
  		</div>
  		<p>
  		<div class="span25">
  	  		<input type="radio" name="checkOutPaymentId" value="EXT_OFFLINE" <#if "EXT_OFFLINE" == checkOutPaymentId>checked="checked"</#if>></input>
  	  		<span>${uiLabelMap.OrderMoneyOrder}</span>
    	</div>
     </div>
   </#if>
                  
   <hr>
   <div class="row">
		<div class="span1">
			<img src="<@ofbizContentUrl>/images/barez/bank_mellat.jpg</@ofbizContentUrl>">
		</div>
		<p>
		<div class="span35">
			<input type="radio" name="checkOutPaymentId" value="EXT_MELLATBANK" checked="checked"/>
			<span>${uiLabelMap.OrderPayWithMellat}</span>
		</div>    
    </div>
      
</div>
  
<#if shipping == true>
<div class="span45 reg-page">

   <div class="reg-header"><h3>${uiLabelMap.OrderWhereShallWeShipIt}</h3></div>
   <div class="row">
     <div class="span3">
       <span>${uiLabelMap.CommonAdd}</span>
       <a href="javascript:submitForm(document.checkoutInfoForm, 'NA', '');" class="btn btn-primary">${uiLabelMap.PartyAddNewAddress}</a>
     </div>
   </div>
   <hr/>
   
   <#if shippingContactMechList?has_content>
     <#list shippingContactMechList as shippingContactMech>
       <#assign shippingAddress = shippingContactMech.getRelatedOne("PostalAddress")>
         <div class="row">
           <div class="span45">
           	 <#if shoppingCart.getShippingContactMechId()?default("") != "" >
	          <input type="radio" name="shipping_contact_mech_id" value="${shippingAddress.contactMechId}" onclick="javascript:submitForm(document.checkoutInfoForm, 'SA', null);"<#if shoppingCart.getShippingContactMechId()?default("") == shippingAddress.contactMechId> checked="checked"</#if>/>
	         <#else>
	         <input type="radio" id="radiooo" name="shipping_contact_mech_id" value="${shippingAddress.contactMechId}" onclick="javascript:submitForm(document.checkoutInfoForm, 'SA', null);" checked="checked"/>
	         </#if>
	          <span>${shippingAddress.contactMechId}
	            <#if shippingAddress.toName?has_content><b>${uiLabelMap.CommonTo}:</b>&nbsp;${shippingAddress.toName}<br /></#if>
	            <#if shippingAddress.attnName?has_content><b>${uiLabelMap.PartyAddrAttnName}:</b>&nbsp;${shippingAddress.attnName}<br /></#if>
	            <#if shippingAddress.address1?has_content>${shippingAddress.address1}<br /></#if>
	            <#if shippingAddress.address2?has_content>${shippingAddress.address2}<br /></#if>
	            <#if shippingAddress.city?has_content>${shippingAddress.city}</#if>
	            <#if shippingAddress.postalCode?has_content>${shippingAddress.postalCode}</#if>
	            <a href="javascript:submitForm(document.checkoutInfoForm, 'EA', '${shippingAddress.contactMechId}');" class="btn btn-primary">${uiLabelMap.CommonUpdate}</a>
	          </span>
           </div>
         </div>
         
       <#if shippingContactMech_has_next>
         <hr />
       </#if>
     </#list>
   </#if>
</div>
</#if>


<#if shipping == true>
<div class="span3 reg-page">
	            
   <div class="reg-header"><h3>${uiLabelMap.OrderHowShallWeShipIt}</h3></div>
                
            
            
            
                 <#if shipping == true>
                  <#list carrierShipmentMethodList as carrierShipmentMethod>
                    <#assign shippingMethod = carrierShipmentMethod.shipmentMethodTypeId + "@" + carrierShipmentMethod.partyId>
                    <div class="row">
	                   <div class="span6">
	                   <#if chosenShippingMethod?exists>    
	                      <input type="radio" name="shipping_method" value="${shippingMethod}" <#if shippingMethod == chosenShippingMethod?default("N@A")>checked="checked"</#if>/>
	                    <#else>
	                      <input type="radio" name="shipping_method" value="${shippingMethod}" checked="checked"/>
	                    </#if>
	                       <span>
	                         <#if shoppingCart.getShippingContactMechId()?exists>
	                           <#assign shippingEst = shippingEstWpr.getShippingEstimate(carrierShipmentMethod)?default(-1)>
	                         </#if>
	                         <#if carrierShipmentMethod.partyId != "_NA_">${carrierShipmentMethod.partyId?if_exists}&nbsp;</#if>${carrierShipmentMethod.description?if_exists}
	                         <#if shippingEst?has_content> - <#if (shippingEst > -1)><@ofbizCurrency amount=shippingEst isoCode=shoppingCart.getCurrency()/><#else>${uiLabelMap.OrderCalculatedOffline}</#if></#if>
	                       </span>
	                     </div>
                     </div>
                      <#if carrierShipmentMethod_has_next>
                      	<hr/>
                      </#if>
                  </#list>
                  <#if !carrierShipmentMethodList?exists || carrierShipmentMethodList?size == 0>
                    <div class="row">
					   <div class="span6>">
	                       <input type="radio" name="shipping_method" value="Default" checked="checked"/>
	                    
	                       <span>${uiLabelMap.OrderUseDefault}.</span>
	                   </div>
                    </div>
                  </#if>
                  
                 <#else/>
                    <input type="hidden" name="shipping_method" value="NO_SHIPPING@_NA_"/>
                    <input type="hidden" name="may_split" value="false"/>
                    <input type="hidden" name="is_gift" value="false"/>
                 </#if>
                
</div>
</div>
</#if>
</form>  

<div class="margin-bottom-20"></div>
<table width="100%">
  <tr valign="top">
    <td>
      &nbsp;<a href="javascript:submitForm(document.checkoutInfoForm, 'CS', '');" class="btn btn-primary">${uiLabelMap.OrderBacktoShoppingCart}</a>
    </td>
    <td align="right">
      <a href="javascript:submitForm(document.checkoutInfoForm, 'DN', '');" class="btn btn-primary">${uiLabelMap.OrderContinueToFinalOrderReview}</a>
    </td>
  </tr>
</table>

</div>
