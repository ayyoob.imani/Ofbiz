<#--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->
<script type="text/javascript">
    function callDocumentByPaginate(info) {
        var str = info.split('~');
        var checkUrl = '<@ofbizUrl>categoryAjaxFired</@ofbizUrl>';
        if(checkUrl.search("http"))
            var ajaxUrl = '<@ofbizUrl>categoryAjaxFired</@ofbizUrl>';
        else
            var ajaxUrl = '<@ofbizUrl>categoryAjaxFiredSecure</@ofbizUrl>';
            
        //jQuerry Ajax Request
        jQuery.ajax({
            url: ajaxUrl,
            type: 'POST',
            data: {"category_id" : str[0], "VIEW_SIZE" : str[1], "VIEW_INDEX" : str[2]},
            error: function(msg) {
                alert("An error occured loading content! : " + msg);
            },
            success: function(msg) {
                jQuery('#div3').html(msg);
				document.getElementById(str[2]).setAttribute("class", "active");
				document.getElementById('Bottom' + ' ' + str[2]).setAttribute("class", "active");
            }
        });
     }
</script>



<#macro paginationControls>
	
    <#assign viewIndexMax = Static["java.lang.Math"].ceil((listSize)?double / viewSize?double)>
    <#assign viewIndexMax = viewIndexMax-1 >
    
 
    	
      <#if (viewIndexMax?int > 0)>
        <div class="product-prevnext">
            <#-- Start Page Select Drop-Down -->
            <#-- select name="pageSelect" onchange="window.location=this[this.selectedIndex].value;">
                <option value="#">${uiLabelMap.CommonPage} ${viewIndex?int} ${uiLabelMap.CommonOf} ${viewIndexMax + 1}</option>
                <#list 0..viewIndexMax as curViewNum>
                     <option value="<@ofbizCatalogAltUrl productCategoryId=productCategoryId viewSize=viewSize viewIndex=(curViewNum?int + 1)/>">${uiLabelMap.CommonGotoPage} ${curViewNum + 1}</option>
                </#list>
            </select -->
            <div class="pagination" >
            
            	
            	
                
            	<#if (listSize?int > 100)>
            		<#if ((lowIndex/viewSize) ?int < 6)>
	            		<ul>
		                <#list 0..9 as curViewNum>
						<li>
							<a style='cursor:pointer' id='${curViewNum}' onclick="callDocumentByPaginate('${productCategoryId}~${viewSize}~${curViewNum?int}')"> ${curViewNum + 1}</a>
		                </li>
		                </#list>
		                <li>
		                <a style='curser:pointer' >...</a>
		                </li>
		                
		                <li>
		                <a style='cursor:pointer' id='${viewIndexMax}' onclick="callDocumentByPaginate('${productCategoryId}~${viewSize}~${viewIndexMax?int}')"> ${viewIndexMax + 1}</a>
		                </li>
						</ul>
					<#elseif ((lowIndex/viewSize) ?int < viewIndexMax- 6)>
						<ul>
						<li>
		                	<a style='cursor:pointer' id='0' onclick="callDocumentByPaginate('${productCategoryId}~${viewSize}~0')"> 1</a>
		                </li>
		                <li>
		                <a style='curser:pointer' >...</a>
		                </li>
		                
		                <#list ((lowIndex/viewSize) ?int -2 )..((lowIndex/viewSize) ?int + 4 ) as curViewNum>
						<li>
							<a style='cursor:pointer' id='${curViewNum}' onclick="callDocumentByPaginate('${productCategoryId}~${viewSize}~${curViewNum?int}')"> ${curViewNum + 1}</a>
		                </li>
		                </#list>
		                <li>
		                <a style='curser:pointer' >...</a>
		                </li>
		                
		                <li>
		                <a style='cursor:pointer' id='${viewIndexMax}' onclick="callDocumentByPaginate('${productCategoryId}~${viewSize}~${viewIndexMax?int}')"> ${viewIndexMax + 1}</a>
		                </li>
						</ul>
					<#else>
						<ul>
						<li>
		                	<a style='cursor:pointer' id='0' onclick="callDocumentByPaginate('${productCategoryId}~${viewSize}~0')"> 1</a>
		                </li>
		                <li>
		                	<a style='curser:pointer' >...</a>
		                </li>
		                
		                <#list viewIndexMax-9..viewIndexMax as curViewNum>
						<li>
							<a style='cursor:pointer' id='${curViewNum}' onclick="callDocumentByPaginate('${productCategoryId}~${viewSize}~${curViewNum?int}')"> ${curViewNum + 1}</a>
		                </li>
		                </#list>
		                
						</ul>
					</#if>
            	<#else>
	                <ul>
	                <#list 0..viewIndexMax as curViewNum>
					<li>
					<a style='cursor:pointer' id='${curViewNum}' onclick="callDocumentByPaginate('${productCategoryId}~${viewSize}~${curViewNum?int}')"> ${curViewNum + 1}</a>
	                     </li>
	                </#list>
					</ul>
				</#if>
				
            	
				</div>
            
            <#-- End Page Select Drop-Down -->
            <#if (viewIndex?int > 0)>
                <#-- a href="<@ofbizUrl>category/~category_id=${productCategoryId}/~VIEW_SIZE=${viewSize}/~VIEW_INDEX=${viewIndex?int - 1}</@ofbizUrl>" class="buttontext">${uiLabelMap.CommonPrevious}</a -->| 
                <a href="javascript: void(0);" onclick="callDocumentByPaginate('${productCategoryId}~${viewSize}~${viewIndex?int - 1}');" class="btn btn-primary">${uiLabelMap.CommonPrevious}</a> |
            </#if>
            <#if ((listSize?int - viewSize?int) > 0)>
                ${lowIndex} ${uiLabelMap.OrderTo} ${highIndex} ${uiLabelMap.CommonOf} ${listSize}
            </#if>
            
			<#if highIndex?int < listSize?int>
            	<#-- | <a href="<@ofbizUrl>category/~category_id=${productCategoryId}/~VIEW_SIZE=${viewSize}/~VIEW_INDEX=${viewIndex?int + 1}</@ofbizUrl>" class="buttontext">${uiLabelMap.CommonNext}</a -->|
            	<a href="javascript: void(0);" onclick="callDocumentByPaginate('${productCategoryId}~${viewSize}~${viewIndex?int + 1}');" class="btn btn-primary">${uiLabelMap.CommonNext}</a>|
            </#if>
            
        </div>
    </#if>
</#macro>

<#macro BottompaginationControls>
	
    <#assign viewIndexMax = Static["java.lang.Math"].ceil((listSize)?double / viewSize?double)>
    <#assign viewIndexMax = viewIndexMax -1>
        	
      <#if (viewIndexMax?int > 0)>
        <div class="product-prevnext">
            <#-- Start Page Select Drop-Down -->
            <#-- select name="pageSelect" onchange="window.location=this[this.selectedIndex].value;">
                <option value="#">${uiLabelMap.CommonPage} ${viewIndex?int} ${uiLabelMap.CommonOf} ${viewIndexMax + 1}</option>
                <#list 0..viewIndexMax as curViewNum>
                     <option value="<@ofbizCatalogAltUrl productCategoryId=productCategoryId viewSize=viewSize viewIndex=(curViewNum?int + 1)/>">${uiLabelMap.CommonGotoPage} ${curViewNum + 1}</option>
                </#list>
            </select -->
            <div class="pagination" >
            
            	
            	
                
            	<#if (listSize?int > 100)>
            		<#if ((lowIndex/viewSize) ?int < 6)>
	            		<ul>
		                <#list 0..9 as curViewNum>
						<li>
							<a style='cursor:pointer' id='${curViewNum}' onclick="callDocumentByPaginate('${productCategoryId}~${viewSize}~${curViewNum?int}')"> ${curViewNum + 1}</a>
		                </li>
		                </#list>
		                <li>
		                <a style='curser:pointer' >...</a>
		                </li>
		                
		                <li>
		                <a style='cursor:pointer' id='${viewIndexMax}' onclick="callDocumentByPaginate('${productCategoryId}~${viewSize}~${viewIndexMax?int}')"> ${viewIndexMax + 1}</a>
		                </li>
						</ul>
					<#elseif ((lowIndex/viewSize) ?int < viewIndexMax- 6)>
						<ul>
						<li>
		                	<a style='cursor:pointer' id='0' onclick="callDocumentByPaginate('${productCategoryId}~${viewSize}~0')"> 1</a>
		                </li>
		                <li>
		                <a style='curser:pointer' >...</a>
		                </li>
		                
		                <#list ((lowIndex/viewSize) ?int -2 )..((lowIndex/viewSize) ?int + 4 ) as curViewNum>
						<li>
							<a style='cursor:pointer' id='${curViewNum}' onclick="callDocumentByPaginate('${productCategoryId}~${viewSize}~${curViewNum?int}')"> ${curViewNum + 1}</a>
		                </li>
		                </#list>
		                <li>
		                <a style='curser:pointer' >...</a>
		                </li>
		                
		                <li>
		                <a style='cursor:pointer' id='${viewIndexMax}' onclick="callDocumentByPaginate('${productCategoryId}~${viewSize}~${viewIndexMax?int}')"> ${viewIndexMax + 1}</a>
		                </li>
						</ul>
					<#else>
						<ul>
						<li>
		                	<a style='cursor:pointer' id='0' onclick="callDocumentByPaginate('${productCategoryId}~${viewSize}~0')"> 1</a>
		                </li>
		                <li>
		                	<a style='curser:pointer' >...</a>
		                </li>
		                
		                <#list viewIndexMax-9..viewIndexMax as curViewNum>
						<li>
							<a style='cursor:pointer' id='${curViewNum}' onclick="callDocumentByPaginate('${productCategoryId}~${viewSize}~${curViewNum?int}')"> ${curViewNum + 1}</a>
		                </li>
		                </#list>
		                
						</ul>
					</#if>
            	<#else>
	                <ul>
	                <#list 0..viewIndexMax as curViewNum>
					<li>
					<a style='cursor:pointer' id='${curViewNum}' onclick="callDocumentByPaginate('${productCategoryId}~${viewSize}~${curViewNum?int}')"> ${curViewNum + 1}</a>
	                     </li>
	                </#list>
					</ul>
				</#if>
				
            	
				</div>
            
            <#-- End Page Select Drop-Down -->
            <#if (viewIndex?int > 0)>
                <#-- a href="<@ofbizUrl>category/~category_id=${productCategoryId}/~VIEW_SIZE=${viewSize}/~VIEW_INDEX=${viewIndex?int - 1}</@ofbizUrl>" class="buttontext">${uiLabelMap.CommonPrevious}</a -->| 
                <a href="javascript: void(0);" onclick="callDocumentByPaginate('${productCategoryId}~${viewSize}~${viewIndex?int - 1}');" class="btn btn-primary">${uiLabelMap.CommonPrevious}</a> |
            </#if>
            <#if ((listSize?int - viewSize?int) > 0)>
                ${lowIndex} ${uiLabelMap.OrderTo} ${highIndex} ${uiLabelMap.CommonOf} ${listSize}
            </#if>
            
			<#if highIndex?int < listSize?int>
            	<#-- | <a href="<@ofbizUrl>category/~category_id=${productCategoryId}/~VIEW_SIZE=${viewSize}/~VIEW_INDEX=${viewIndex?int + 1}</@ofbizUrl>" class="buttontext">${uiLabelMap.CommonNext}</a -->|
            	<a href="javascript: void(0);" onclick="callDocumentByPaginate('${productCategoryId}~${viewSize}~${viewIndex?int + 1}');" class="btn btn-primary">${uiLabelMap.CommonNext}</a>|
            </#if>
            
        </div>
    </#if>
</#macro>

<div class="span65">
<#if productCategory?exists>
    <#assign categoryName = categoryContentWrapper.get("CATEGORY_NAME")?if_exists/>
    <#assign categoryDescription = categoryContentWrapper.get("DESCRIPTION")?if_exists/>
    <#if categoryName?has_content || categoryDescription?has_content >    
    <#if categoryName?has_content>
        <h1 class="pagination-centered">${categoryName}</h1>
    </#if>
    <#if categoryDescription?has_content>
        <h2 class="pagination-centered">${categoryDescription}</h2>
    </#if>
    <div class="solid-line"></div>
    </#if>
    
    <#if hasQuantities?exists || searchInCategory?default("Y") == "Y">
	    <#-- add this category products with default quantities-->
	    <#if hasQuantities?exists>
	      <form method="post" action="<@ofbizUrl>addCategoryDefaults<#if requestAttributes._CURRENT_VIEW_?exists>/${requestAttributes._CURRENT_VIEW_}</#if></@ofbizUrl>" name="thecategoryform" style='margin: 0;'>
	        <input type='hidden' name='add_category_id' value='${productCategory.productCategoryId}'/>
	        <#if requestParameters.product_id?exists><input type='hidden' name='product_id' value='${requestParameters.product_id}'/></#if>
	        <#if requestParameters.category_id?exists><input type='hidden' name='category_id' value='${requestParameters.category_id}'/></#if>
	        <#if requestParameters.VIEW_INDEX?exists><input type='hidden' name='VIEW_INDEX' value='${requestParameters.VIEW_INDEX}'/></#if>
	        <#if requestParameters.SEARCH_STRING?exists><input type='hidden' name='SEARCH_STRING' value='${requestParameters.SEARCH_STRING}'/></#if>
	        <#if requestParameters.SEARCH_CATEGORY_ID?exists><input type='hidden' name='SEARCH_CATEGORY_ID' value='${requestParameters.SEARCH_CATEGORY_ID}'/></#if>
	        <p>
	        <a href="javascript:document.thecategoryform.submit()" class="btn btn-primary"><span style="white-space: nowrap;">${uiLabelMap.ProductAddProductsUsingDefaultQuantities}</span></a>
	        </p>
	      </form>
	    </#if>
	    <#-- search in this category-->
	    <#if searchInCategory?default("Y") == "Y">
	    	<p>
	        <a href="<@ofbizUrl>advancedsearch?SEARCH_CATEGORY_ID=${productCategory.productCategoryId}</@ofbizUrl>" class="btn btn-primary" >${uiLabelMap.ProductSearchInCategory}</a>
	        </p>
	    </#if>
	    
	    <div class="solid-line"></div>
    </#if>
    
    <#assign longDescription = categoryContentWrapper.get("LONG_DESCRIPTION")?if_exists/>
    <#assign categoryImageUrl = categoryContentWrapper.get("CATEGORY_IMAGE_URL")?if_exists/>
    <#if categoryImageUrl?string?has_content || longDescription?has_content>
      <div>
      
        <#if categoryImageUrl?string?has_content>
          <#assign height=100/>
          <p><img src='<@ofbizContentUrl>${categoryImageUrl}</@ofbizContentUrl>' vspace='5' hspace='5' align='left' class='cssImgLarge' /></p>
        </#if>
        <#if longDescription?has_content>
          <p>${longDescription}</p>
        </#if>
      </div>
      <div class="solid-line"></div>
  </#if>
  
</#if>
<#--
<#if productCategoryLinkScreen?has_content && productCategoryLinks?has_content>
    <div class="productcategorylink-container">
        <#list productCategoryLinks as productCategoryLink>
            ${setRequestAttribute("productCategoryLink",productCategoryLink)}
            ${screens.render(productCategoryLinkScreen)}
        </#list>
    </div>
</#if>
-->
<#if productCategoryMembers?has_content>
    <#-- Pagination -->
    <#if paginateEcommerceStyle?exists>
        <@paginationControls/>
    <#else>
        <#include "component://common/webcommon/includes/htmlTemplate.ftl"/>
        <#assign commonUrl = "category?category_id="+ parameters.category_id?if_exists + "&"/>
        <#--assign viewIndex = viewIndex - 1/-->
        <#assign viewIndexFirst = 0/>
        <#assign viewIndexPrevious = viewIndex - 1/>
        <#assign viewIndexNext = viewIndex + 1/>
        <#assign viewIndexLast = Static["java.lang.Math"].floor(listSize/viewSize)/>
        <#assign messageMap = Static["org.ofbiz.base.util.UtilMisc"].toMap("lowCount", lowIndex, "highCount", highIndex, "total", listSize)/>
        <#assign commonDisplaying = Static["org.ofbiz.base.util.UtilProperties"].getMessage("CommonUiLabels", "CommonDisplaying", messageMap, locale)/>
        <@nextPrev commonUrl=commonUrl ajaxEnabled=false javaScriptEnabled=false paginateStyle="nav-pager" paginateFirstStyle="nav-first" viewIndex=viewIndex highIndex=highIndex listSize=listSize viewSize=viewSize ajaxFirstUrl="" firstUrl="" paginateFirstLabel="" paginatePreviousStyle="nav-previous" ajaxPreviousUrl="" previousUrl="" paginatePreviousLabel="" pageLabel="" ajaxSelectUrl="" selectUrl="" ajaxSelectSizeUrl="" selectSizeUrl="" commonDisplaying=commonDisplaying paginateNextStyle="nav-next" ajaxNextUrl="" nextUrl="" paginateNextLabel="" paginateLastStyle="nav-last" ajaxLastUrl="" lastUrl="" paginateLastLabel="" paginateViewSizeLabel="" />
    </#if>
    
      <#assign numCol = numCol?default(1)>
      <#assign numCol = numCol?number>
      <#assign tabCol = 1>
     <#-- <div
      <#if categoryImageUrl?string?has_content>
        style="position: relative; margin-top: ${height}px;"
      </#if>
      class="productsummary-container<#if (numCol?int > 1)> matrix</#if>">
    <#--  <#if (numCol?int > 1)>
        <table>
      </#if> -->
      <#-- <ul class="thumbnails"> -->
      <hr>
        <#list productCategoryMembers as productCategoryMember>
          
          <#if (numCol?int == 1)>
            ${setRequestAttribute("optProductId", productCategoryMember.productId)}
            ${setRequestAttribute("productCategoryMember", productCategoryMember)}
            ${setRequestAttribute("listIndex", productCategoryMember_index)}
            
            ${screens.render(productsummaryScreen)}
          <#else>
          <#-- else is never renderd!!!! this are the big boxes>
             <#-- <#if (tabCol?int = 1)><tr></#if>
                  <td>-->
                  <li class="span35">
                      ${setRequestAttribute("optProductId", productCategoryMember.productId)}
                      ${setRequestAttribute("productCategoryMember", productCategoryMember)}
                      ${setRequestAttribute("listIndex", productCategoryMember_index)}
                      ${screens.render(productsummaryScreen)}
                  </li>
              <#--    </td>
               <#if (tabCol?int = numCol)></tr></#if>
              <#assign tabCol = tabCol+1><#if (tabCol?int > numCol)><#assign tabCol = 1></#if> -->
           </#if>
        </#list>
        <#-- </ul> -->
   <#--   <#if (numCol?int > 1)>
        </table>
      </#if> 
      </div> -->
    <#if paginateEcommerceStyle?exists>
        <@BottompaginationControls/>
    </#if>
<#else>
    <hr />
    <div>${uiLabelMap.ProductNoProductsInThisCategory}</div>
</#if>

</div>
