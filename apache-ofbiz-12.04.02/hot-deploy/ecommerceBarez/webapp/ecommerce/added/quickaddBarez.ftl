<#--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->

<div class="span65">

<#if productCategory?has_content>

<div class="row">
  <div class="span3">
	 <p>
        <h2>${productCategory.categoryName?if_exists}</h2>
     </p>
  </div>   
  <div class="span35">    
        <form name="choosequickaddform" method="post" action="<@ofbizUrl>quickadd</@ofbizUrl>" class="pull-left" >
          <select name='category_id'>
            <option value='${productCategory.productCategoryId}'>${productCategory.categoryName?if_exists}</option>
            <option value='${productCategory.productCategoryId}'>--</option>
            <#list quickAddCats as quickAddCatalogId>
              <#assign loopCategory = delegator.findByPrimaryKeyCache("ProductCategory", Static["org.ofbiz.base.util.UtilMisc"].toMap("productCategoryId", quickAddCatalogId))>
              <#if loopCategory?has_content>
                <option value='${quickAddCatalogId}'>${loopCategory.categoryName?if_exists}</option>
              </#if>
            </#list>
          </select>
          <div class="pull-right"><a href="javascript:document.choosequickaddform.submit()" class="btn btn-primary">${uiLabelMap.ProductChooseQuickAddCategory}</a></div>
        </form>
   </div>
    <#if productCategory.categoryImageUrl?exists || productCategory.longDescription?exists>
          <div class="span65 ">
          <div class="solid-line">
          <#-- no image !!! -->
          <#-->
            <#if productCategory.categoryImageUrl?exists>
              <img src="<@ofbizContentUrl>${productCategory.categoryImageUrl}</@ofbizContentUrl>" vspace="5" hspace="5" class="cssImgLarge" alt="" />
            </#if>
          -->
            ${productCategory.longDescription?if_exists}
          </div>
          </div>
    </#if>
  
</div>
</#if>

<#if productCategoryMembers?exists && 0 < productCategoryMembers?size>

  <form method="post" action="<@ofbizUrl>addtocartbulk</@ofbizUrl>" name="bulkaddform" style='margin: 0;'>
    <input type='hidden' name='category_id' value='${categoryId}' />
    <p>
    <div class="row">
	    <div class="pull-left">
	      <a href="javascript:document.bulkaddform.submit()" class="btn btn-primary">${uiLabelMap.OrderAddAllToCart}</a>
	    </div>
	</div>
	</p>
    <hr>
      <#list productCategoryMembers as productCategoryMember>
        <#assign product = productCategoryMember.getRelatedOneCache("Product")>
        
          <div class="row">
            ${setRequestAttribute("optProductId", productCategoryMember.productId)}
            ${screens.render(quickaddsummaryScreen)}
          </div>
            <hr>
        
      </#list>
    
     <div class="row">
	    <div class="pull-left">
	      <a href="javascript:document.bulkaddform.submit()" class="btn btn-primary">${uiLabelMap.OrderAddAllToCart}</a>
	    </div>
	</div>
	
  </form>

<#else>
  <table border="0" cellpadding="2">
    <tr><td colspan="2"><hr class='sepbar'/></td></tr>
    <tr>
      <td>
        <div class='tabletext'>${uiLabelMap.ProductNoProductsInThisCategory}.</div>
      </td>
    </tr>
  </table>
</#if>
</div>
