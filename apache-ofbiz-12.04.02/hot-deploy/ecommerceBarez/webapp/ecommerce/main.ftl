<#--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->

<#-- Render the category page -->
<#if requestAttributes.productCategoryId?has_content>
	<#-- we don't need carusoul here-->
	<#-- <div class="span9">
  		${screens.render("component://ecommerce/widget/CatalogScreens.xml#bestSellingCategory")}
  	</div> -->
  	      ${screens.render("component://ecommerceBarez/widget/CatalogScreens.xml#category-include")}
	
	<#-- we make two permanent side bars-->
	 <#--
	<div class="span2">
		  ${screens.render("component://ecommerce/widget/CommonScreens.xml#rightbar")}
		  
	</div> -->
	
<#else>
<div class="span65"
  <center><h2>${uiLabelMap.EcommerceNoPROMOTIONCategory}</h2></center>
</div>
</#if>

	