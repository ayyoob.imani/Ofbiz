<#--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->
<script>
$(document).ready(function(){
  $(".totop").hide();

  $(function(){
    $(window).scroll(function(){
      if ($(this).scrollTop()>600)
      {
        $('.totop').slideDown();
      } 
      else
      {
        $('.totop').slideUp();
      }
    });

    $('.totop a').click(function (e) {
      e.preventDefault();
      $('body,html').animate({scrollTop: 0}, 500);
    });

  });
});
</script>

<#assign nowTimestamp = Static["org.ofbiz.base.util.UtilDateTime"].nowTimestamp()>

<footer>
	<hr>	
	  <div class="row well no_margin_left">
	    <!--  <div>
	        <a href="http://jigsaw.w3.org/css-validator/"><img style="border:0;width:88px;height:31px" src="<@ofbizContentUrl>/images/vcss.gif</@ofbizContentUrl>" alt="Valid CSS!"/></a>
	        <a href="http://validator.w3.org/check?uri=referer"><img style="border:0;width:88px;height:31px" src="<@ofbizContentUrl>/images/valid-xhtml10.png</@ofbizContentUrl>" alt="Valid XHTML 1.0!"/></a>
	      </div> 
		 --> 
	      <br />
	      <div class="tabletext">
	        <#-- <a href="http://ofbiz.apache.org">${uiLabelMap.EcommerceAboutUs}</a> -->
	        <div class="tabletext pull-left">Copyright (c) 2001-${nowTimestamp?string("yyyy")} The HISIS Company - <a href="http://www.hisis.ir" class="tabletext">www.hisis.ir</a></div>
	        <#-- <div class="tabletext">Powered by<a href="http://hisis.ir" class="tabletext">hisis</a></div> -->
	        
	      </div>
	      <br />
	    <!--  <div class="tabletext"><a href="<@ofbizUrl>policies</@ofbizUrl>">${uiLabelMap.EcommerceSeeStorePoliciesHere}</a></div>-->
	  </div>
	  
	  <span class="totop" style="display: block;"><a href="#"><i class="icon-chevron-up"></i></a></span>

</footer>
