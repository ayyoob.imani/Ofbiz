package org.ofbiz.accounting.thirdparty.bankmellat;




import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;






import javax.servlet.http.HttpSession;

import javolution.util.FastMap;

import com.bps.sw.pgw.service.*;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilFormatOut;
import org.ofbiz.base.util.UtilHttp;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.transaction.GenericTransactionException;
import org.ofbiz.entity.transaction.TransactionUtil;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.order.order.OrderChangeHelper;
import org.ofbiz.product.store.ProductStoreWorker;
//import org.python.core.exceptions;
import org.ofbiz.service.GenericServiceException;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ModelService;
import org.ofbiz.service.ServiceAuthException;
import org.ofbiz.service.ServiceValidationException;
//import org.python.core.exceptions;

public class MellatPayEvents {

	public static final String resource = "AccountingUiLabels";
    public static final String resourceErr = "AccountingErrorUiLabels";
    public static final String resourceErrSms = "CommonErrorUiLabels";
    public static final String commonResource = "CommonUiLabels";
    public static final String module = MellatPayEvents.class.getName();
	
	public static String mellatPayRequest(HttpServletRequest request, HttpServletResponse response)
	{
		Locale locale = UtilHttp.getLocale(request);
		Delegator delegator = (Delegator) request.getAttribute("delegator");
	    String orderId =(String) request.getAttribute("orderId");
	    HttpSession session = request.getSession();
	    
	    GenericValue orderHeader = null;
	    
	    try{
	    	orderHeader = delegator.findOne("OrderHeader", UtilMisc.toMap("orderId", orderId), false);
	    }
	    catch(GenericEntityException e){
	    	Debug.logError(e, "Cannot get the order header for order: " + orderId, module);
            request.setAttribute("_ERROR_MESSAGE_", UtilProperties.getMessage(resourceErr, "MellatPayEvents.problemsGettingOrderHeader", locale));
            return "error";
	    }
	    
	 // get the order total
        Long orderTotal = orderHeader.getBigDecimal("grandTotal").longValue();
        
        long terminalId = 910226;
        String userName = "barezpa";
        String userPassword = "ba19re";
        
        Date date = new Date();
		String currentDate = new SimpleDateFormat("yyyyMMdd").format(date);
		String currentTime = new SimpleDateFormat("HHmmss").format(date);
		//String callBackUrl = "http://irspc.ir/ecommerce/control/mellatPayNotify";
		String callBackUrl = "http://irspc.ir/ecommerceBarez/control/mellatPayNotify";
		
		IPaymentGateway service = new PaymentGatewayImplService().getPaymentGatewayImplPort();
		
		long orderIdLong = Long.parseLong(orderId, 36);
		String resultMessage = null;
		try{
			resultMessage = service.bpPayRequest(terminalId, userName, userPassword, orderIdLong, orderTotal , currentDate, currentTime, "", callBackUrl, 0);
		}
		catch(Exception e){
			Debug.logError("error requesting mellat pay: " + e, module);
			request.setAttribute("_ERROR_MESSAGE_", "could'nt send a pay request to mellat Bank");
			return "error";
		}
		Debug.logWarning(resultMessage, module);
		StringTokenizer stringtokenizer = new StringTokenizer(resultMessage, ",");
		
		String[] results = new String[2];
		
		int i=0;
		while(stringtokenizer.hasMoreTokens()){
			results[i]=(String) stringtokenizer.nextElement();
			i++;
		}
		
		if(results[0].compareTo("0")==0){
			request.setAttribute("refId", results[1]);
			session.setAttribute("payRefrenceId", results[1]);
			session.setAttribute("payOrderId", orderId);
			return "success";
		}
		else{
			Debug.logError("bad response to bppayrequest response is: " + results[0], module );
			request.setAttribute("_ERROR_MESSAGE_", "internal error when trying to request payment from mellat bank please try later");
			return "error";
		}
		
        
	}
	
	public static String mellatPayNotify(HttpServletRequest request, HttpServletResponse response) {
        Locale locale = UtilHttp.getLocale(request);
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
        HttpSession session = request.getSession();
        GenericValue userLogin = (GenericValue) session.getAttribute("userLogin");
        Map <String, Object> parametersMap = UtilHttp.getParameterMap(request);
        
        
        String refId = request.getParameter("RefId");
        Debug.logWarning("reference id is: " + refId, module);
        String resCode = request.getParameter("ResCode");
        Debug.logWarning("result code is: " + resCode, module);
        long saleOrderId = Long.valueOf(request.getParameter("SaleOrderId")).longValue();
        Debug.logWarning("sale orderId is: " + saleOrderId, module);
        long saleRefrenceId = 0;
        
        try{
        	 saleRefrenceId = Long.valueOf(request.getParameter("SaleReferenceId")).longValue();
        }
        catch(Exception e){
        	Debug.logWarning("can not change this sale refrence Id to long: " + request.getParameter("SaleReferenceId"), module);
        }
        Debug.logWarning("sale reference id is: " + saleRefrenceId, module);
        
        long terminalId = 910226;
        String userName = "barezpa";
        String userPassword = "ba19re";
        
        String orderId = (String)session.getAttribute("payOrderId");
        
        request.setAttribute("orderId", orderId);
        
        for(String name : parametersMap.keySet()) {
            String value = request.getParameter(name);
            Debug.logError("### Param: " + name + " => " + value, module);
        }
        // get the user
        if (userLogin == null) {
            String userLoginId = "system";
            try {
                userLogin = delegator.findOne("UserLogin", UtilMisc.toMap("userLoginId", userLoginId), false);
            } catch (GenericEntityException e) {
                Debug.logError(e, "Cannot get UserLogin for: " + userLoginId + "; cannot continue", module);
                request.setAttribute("_ERROR_MESSAGE_", UtilProperties.getMessage(resourceErr, "mellatPayEvents.problemsGettingAuthenticationUser", locale));
                return "error";
            }
        }
        // get the order header
        GenericValue orderHeader = null;
        if (UtilValidate.isNotEmpty(orderId)) {
            try {
                orderHeader = delegator.findOne("OrderHeader", UtilMisc.toMap("orderId", orderId), false);
            } catch (GenericEntityException e) {
                Debug.logError(e, "Cannot get the order header for order: " + orderId, module);
                request.setAttribute("_ERROR_MESSAGE_", UtilProperties.getMessage(resourceErr, "mellatPayEvents.problemsGettingOrderHeader", locale));
                return "error";
            }
        } else {
            Debug.logError("mellatPay did not callback with a valid orderId!", module);
            request.setAttribute("_ERROR_MESSAGE_", UtilProperties.getMessage(resourceErr, "mellatPayEvents.noValidOrderIdReturned", locale));
            return "error";
        }
        if (orderHeader == null) {
            Debug.logError("Cannot get the order header for order: " + orderId, module);
            request.setAttribute("_ERROR_MESSAGE_", UtilProperties.getMessage(resourceErr, "mellatPayEvents.problemsGettingOrderHeader", locale));
            return "error";
        }
        
        IPaymentGateway service = new PaymentGatewayImplService().getPaymentGatewayImplPort();
        
        // attempt to start a transaction
        boolean okay = false;
        boolean beganTransaction = false;
        boolean payed = false;
        
        
        GenericValue productStore = org.ofbiz.product.store.ProductStoreWorker.getProductStore(request);
        
        
        Map<String, Object> smsParams = FastMap.newInstance();
        smsParams.put("sendTo", userLogin.get("userLoginId"));
        smsParams.put("userName", "barezpakhsh");
        smsParams.put("sederNumber", "10001878");
        smsParams.put("content", "آقای "+session.getAttribute("autoName")+" یک سفارش با شماره "+orderId+"برای شما ثبت شد."+"\n"+ productStore.get("title"));
        smsParams.put("isUTF8", "true");
        smsParams.put("password", "bpkerman");
        try {
            beganTransaction = TransactionUtil.begin();
            // authorized
            if (refId.compareTo((String)session.getAttribute("payRefrenceId"))==0 && saleOrderId == Long.parseLong(orderId, 36)) {
            	Debug.logWarning("refId and salOrderId ok", module);
            	if( "0".equals(resCode)   )
            	{
            		Debug.logWarning("result code is ok", module);
                	
                   String responseMessage = service.bpVerifyRequest(terminalId, userName, userPassword,
                      		saleOrderId, saleOrderId, saleRefrenceId);
                   
                   if(responseMessage.equals("0")){
                	   Debug.logWarning("verified successfully", module);
                	   String fineResponse = service.bpSettleRequest(terminalId, userName, userPassword,
                			   saleOrderId, saleOrderId, saleRefrenceId);
                	   Debug.logWarning("settle code is: " + fineResponse, module);
                	   
                	   okay = OrderChangeHelper.approveOrder(dispatcher, userLogin, orderId);
                	   Debug.logWarning("approve status: " + okay, module);
                	   payed = true;
                	   
                	   
                   }
                   else{
                	   okay = OrderChangeHelper.cancelOrder(dispatcher, userLogin, orderId);
                	   
                   }
                        
                 }
            	else{
            		okay = OrderChangeHelper.cancelOrder(dispatcher, userLogin, orderId);
            	}
            	
            // cancelled
            } else {
            	
        		request.setAttribute("_ERROR_MESSAGE_", UtilProperties.getMessage(resourceErr, "mellatPayEvents.noSuchAnorderfound", locale));
                
                
                
            }
            if (okay) {
                // set the payment preference
                okay = setPaymentPreferences(delegator, dispatcher, userLogin, orderId, request);
            }
        } catch (Exception e) {
            String errMsg = "Error handling mellatpay notification";
            Debug.logError(e, errMsg, module);
            try {
                TransactionUtil.rollback(beganTransaction, errMsg, e);
                request.setAttribute("_ERROR_MESSAGE_", UtilProperties.getMessage(resourceErr, "mellatPayEvents.problemApplyingYourRequestCallUsImmediatlyNum1", locale));
                
            } catch (GenericTransactionException gte2) {
                Debug.logError(gte2, "Unable to rollback transaction", module);
                request.setAttribute("_ERROR_MESSAGE_", UtilProperties.getMessage(resourceErr, "mellatPayEvents.problemApplyingYourRequestCallUsImmediatlyNum2", locale));
                
            }
        } finally {
            if (!okay) {
                try {
                    TransactionUtil.rollback(beganTransaction, "Failure in processing mellatPay callback", null);
                    request.setAttribute("_ERROR_MESSAGE_", UtilProperties.getMessage(resourceErr, "mellatPayEvents.problemApplyingYourRequestCallUsImmediatlyNum3", locale));
                    
                } catch (GenericTransactionException gte) {
                    Debug.logError(gte, "Unable to rollback transaction", module);
                    request.setAttribute("_ERROR_MESSAGE_", UtilProperties.getMessage(resourceErr, "mellatPayEvents.problemApplyingYourRequestCallUsImmediatlyNum3", locale));
                    
                }
            } else {
                try {
                    TransactionUtil.commit(beganTransaction);
                    
					 
                } catch (GenericTransactionException gte) {
                    Debug.logError(gte, "Unable to commit transaction", module);
                    request.setAttribute("_ERROR_MESSAGE_", UtilProperties.getMessage(resourceErrSms, "mellatPayEvents.problemApplyingYourRequestCallUsImmediatlyNum4", locale));
                }
                
            }
        }
        
        
        if (okay) {
        	
        	
        	
            // attempt to release the offline hold on the order (workflow)
            OrderChangeHelper.releaseInitialOrderHold(dispatcher, orderId);
            // call the email confirm service
            Map<String, Object> emailContext = UtilMisc.toMap("orderId", orderId, "userLogin", userLogin);
            try {
                dispatcher.runSync("sendOrderConfirmation", emailContext);
            } catch (GenericServiceException e) {
                Debug.logError(e, "Problems sending email confirmation", module);
            }
        }
        if(!payed)
        {
        	request.setAttribute("add_all", true);
        	Debug.logWarning("payment not successfull", module);
        	return "failure";
        }
        else{
        	try{
            	dispatcher.runAsync("sendSMS", smsParams);
            	}
            	catch (ServiceAuthException e) {
        			Debug.logError("couldnt send sms " , module);
        			request.setAttribute("_ERROR_MESSAGE_", UtilProperties.getMessage(resourceErrSms, "smsEvents.error_accoured_when_trying_to_send_confirmtion_sms", locale));
        			
        			e.printStackTrace();
        			
        		} catch (ServiceValidationException e) {
        			
        			Debug.logError("couldnt send sms " , module);
        			request.setAttribute("_ERROR_MESSAGE_", UtilProperties.getMessage(resourceErrSms, "smsEvents.error_accoured_when_trying_to_send_confirmtion_sms", locale));
        			e.printStackTrace();
        			
        		} catch (GenericServiceException e) {
        			
        			Debug.logError("couldnt send sms " , module);
        			request.setAttribute("_ERROR_MESSAGE_", UtilProperties.getMessage(resourceErr, "smsEvents.error_accoured_when_trying_to_send_confirmtion_sms", locale));
        			
        			e.printStackTrace();
        			
        		}
        }
        return "success";
    }

    private static boolean setPaymentPreferences(Delegator delegator, LocalDispatcher dispatcher, GenericValue userLogin, String orderId, HttpServletRequest request) {
        Debug.logVerbose("Setting payment preferences..", module);
        List<GenericValue> paymentPrefs = null;
        try {
            Map<String, String> paymentFields = UtilMisc.toMap("orderId", orderId, "statusId", "PAYMENT_NOT_RECEIVED");
            paymentPrefs = delegator.findByAnd("OrderPaymentPreference", paymentFields);
        } catch (GenericEntityException e) {
            Debug.logError(e, "Cannot get payment preferences for order #" + orderId, module);
            return false;
        }
        if (paymentPrefs.size() > 0) {
            for(GenericValue pref : paymentPrefs) {
                boolean okay = setPaymentPreference(dispatcher, userLogin, pref, request);
                if (!okay) {
                    return false;
                }
            }
        }
        return true;
    }

    private static boolean setPaymentPreference(LocalDispatcher dispatcher, GenericValue userLogin, GenericValue paymentPreference, HttpServletRequest request) {
        Locale locale = UtilHttp.getLocale(request);
        String paymentStatus = request.getParameter("ResCode");
        
        
        //String paymentAmount = request.getParameter("authAmount");
        //Long paymentDate = new Long(request.getParameter("transTime"));
        //String transactionId = request.getParameter("transId");
        //String gatewayFlag = request.getParameter("rawAuthCode");
        //String avs = request.getParameter("AVS");
        List<GenericValue> toStore = new LinkedList<GenericValue>();
        java.sql.Timestamp authDate = null;
        
        long saleRefrenceId = Long.valueOf(request.getParameter("SaleReferenceId")).longValue();
        
        
        try {
            authDate = new java.sql.Timestamp(new Date().getTime());
            
        } catch (Exception e) {
            //Debug.logError(e, "Cannot create date from long: " + paymentDate, module);
            authDate = UtilDateTime.nowTimestamp();
        }
        //paymentPreference.set("maxAmount", new BigDecimal(paymentAmount));
        paymentPreference.set("paymentRefrenceId",saleRefrenceId);
        if ("0".equals(paymentStatus)) {
            paymentPreference.set("statusId", "PAYMENT_RECEIVED");
        } else  {
            paymentPreference.set("statusId", "PAYMENT_CANCELLED");
        } 
        toStore.add(paymentPreference);
        Delegator delegator = paymentPreference.getDelegator();
        // create the PaymentGatewayResponse
        String responseId = delegator.getNextSeqId("PaymentGatewayResponse");
        GenericValue response = delegator.makeValue("PaymentGatewayResponse");
        response.set("paymentGatewayResponseId", responseId);
        response.set("paymentServiceTypeEnumId", "PRDS_PAY_EXTERNAL");
        response.set("orderPaymentPreferenceId", paymentPreference.get("orderPaymentPreferenceId"));
        response.set("paymentMethodTypeId", paymentPreference.get("paymentMethodTypeId"));
        response.set("paymentMethodId", paymentPreference.get("paymentMethodId"));
        // set the auth info
        //response.set("amount", new BigDecimal(paymentAmount));
        //response.set("referenceNum", request.getParameter("SaleReferenceId"));
        response.set("gatewayCode", paymentStatus);
        //response.set("gatewayFlag", gatewayFlag);
        response.set("transactionDate", authDate);
        //response.set("gatewayAvsResult", avs);
        //response.set("gatewayCvResult", avs.substring(0, 1));

        toStore.add(response);
        try {
            delegator.storeAll(toStore);
        } catch (GenericEntityException e) {
            Debug.logError(e, "Cannot set payment preference/payment info", module);
            return false;
        }
        /*
        // create a payment record too
        Map<String, Object> results = null;
        try {
            String comment = UtilProperties.getMessage(resource, "AccountingPaymentReceiveViaWorldPay", locale);
            results = dispatcher.runSync("createPaymentFromPreference", UtilMisc.toMap("userLogin", userLogin,
                    "orderPaymentPreferenceId", paymentPreference.get("orderPaymentPreferenceId"), "comments", comment));
        } catch (GenericServiceException e) {
            Debug.logError(e, "Failed to execute service createPaymentFromPreference", module);
            request.setAttribute("_ERROR_MESSAGE_", UtilProperties.getMessage(resourceErr, "mellatPayEvents.failedToExecuteServiceCreatePaymentFromPreference", locale));
            return false;
        }
        if ((results == null) || (results.get(ModelService.RESPONSE_MESSAGE).equals(ModelService.RESPOND_ERROR))) {
            Debug.logError((String) results.get(ModelService.ERROR_MESSAGE), module);
            request.setAttribute("_ERROR_MESSAGE_", (String) results.get(ModelService.ERROR_MESSAGE));
            return false;
        }*/
        return true;
    }
   
}
