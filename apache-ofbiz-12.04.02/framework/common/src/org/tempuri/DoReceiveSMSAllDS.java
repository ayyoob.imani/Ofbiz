
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="uUsername" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "uUsername",
    "uPassword"
})
@XmlRootElement(name = "doReceiveSMSAllDS")
public class DoReceiveSMSAllDS {

    protected String uUsername;
    protected String uPassword;

    /**
     * Gets the value of the uUsername property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUUsername() {
        return uUsername;
    }

    /**
     * Sets the value of the uUsername property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUUsername(String value) {
        this.uUsername = value;
    }

    /**
     * Gets the value of the uPassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUPassword() {
        return uPassword;
    }

    /**
     * Sets the value of the uPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUPassword(String value) {
        this.uPassword = value;
    }

}
