package org.ofbiz.common.sms;


import java.util.Map;

import org.ofbiz.base.util.Debug;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.ServiceUtil;
import org.tempuri.Sms;
import org.tempuri.SmsSoap;


public class SmsServices {
	public final static String module = SmsServices.class.getName();

	public static Map sendSms(DispatchContext dctx, Map context){
		String module = SmsServices.class.getName();
		
		String userName = (String) context.get("userName");
		String password = (String) context.get("password");
		String senderNumber = (String) context.get("sederNumber");
		String content = (String) context.get("content");
		String sendTo = (String) context.get("sendTo");
		boolean isUtf8 = Boolean.valueOf((String) context.get("isUTF8"));
		
		Debug.logInfo("userName: " + userName + "password: " + password + "senderNom"+ senderNumber + "content: " + content + "sentdo:"+sendTo +"isutf8: "+ isUtf8 , module);
		
		Sms smsclien = new Sms();
		SmsSoap soapClient = smsclien.getSmsSoap();
		String result = soapClient.doSendSMS(userName, password, senderNumber, sendTo, content, isUtf8);
		Debug.logWarning("an sms send request has done the result code is: "+result, module);
		
		Map resultmap ;
		if("Send OK".equals(result.substring(0, 7))){
			resultmap = ServiceUtil.returnSuccess();
			
		}
		else{
			resultmap = ServiceUtil.returnError("sms didnt send successfully du to problem in sms panel.");
			
		}
		return resultmap;
	}
}

